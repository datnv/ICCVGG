import numpy as np
import ic
import ic_input
import tensorflow as tf

nbclasses=2#
crop_size=96



def get_y(x_batch,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training):
    y = y_conv.eval(
        feed_dict={x: x_batch
            , keep_prob_fc1: 1.0
            , keep_prob_fc2: 1.0
            , is_training: False})
    return y


def get_emotion_center_crop(x_batch,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std):
    return _get_emotion(x_batch, 1,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std)


def get_emotion_ten_crops(x_batch,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std):
    return _get_emotion(x_batch, 10,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std)


def get_emotion_more_crops(x_batch,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std):
    return _get_emotion(x_batch, 648,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std)


def _get_emotion(x_batch,nb_crop,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std):
    set = []

    if nb_crop == 1:
        set = ic_input.center_crop(x_batch,crop_size)
    elif nb_crop == 10:
        set = ic_input.ten_crop(x_batch,crop_size)
    elif nb_crop == 648:
        set = ic_input.more_crop(x_batch,crop_size)

    y_average = np.zeros((np.shape(x_batch)[0], nbclasses))
    num_average = 0
    for j in range(len(set)):
        y_sub = get_y(ic_input.pre_processing(set[j],X_mean,X_std),y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training)
        y_average += y_sub
        num_average+=1
    y_average /= float(num_average)
    return y_average

def compute_accuracy_in_test(images, labels,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training):
    sum = 0
    num = 0
    X_mean=np.load('file npy/X_mean.npy')
    X_std = np.load('file npy/X_std.npy')
    result=[]
    for i in range(11):
        x_batch = images[i * (len(images) // 10):(i + 1) * (len(images) // 10)]
        y_batch = labels[i * (len(images) // 10):(i + 1) * (len(images) // 10)]
        y_predict=get_emotion_ten_crops(x_batch,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std)
        correct_prediction = np.equal(np.argmax(y_predict, 1), np.argmax(y_batch, 1)).astype(np.float32)
        sum += np.sum(correct_prediction)
        num += np.size(correct_prediction)
    print('accuracy in test = ' +
          str(sum / num))

def compute_accuracy_in_test_top(images, labels,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,fraction):
    sum=0
    num=0
    X_mean=np.load('file npy/X_mean.npy')
    X_std = np.load('file npy/X_std.npy')
    probabilities=np.empty((0,2))
    for i in range(11):
        x_batch = images[i * (len(images) // 10):(i + 1) * (len(images) // 10)]
        y_batch = labels[i * (len(images) // 10):(i + 1) * (len(images) // 10)]
        y_predict=get_emotion_ten_crops(x_batch,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std)
        y_predict_tensor=tf.constant(y_predict)
        y_softmax_tensor=tf.nn.softmax(y_predict_tensor)
        y_softmax=y_softmax_tensor.eval()
        probabilities=np.vstack((probabilities,y_softmax))
    accuracy = np.average(np.equal(np.argmax(probabilities, 1), np.argmax(labels, 1)).astype(np.float32))
    print('overral accuracy: '+str(accuracy))
    total_img=np.shape(images)[0]
    num_img=int(total_img*float(fraction))
    probabilities_max=np.max(probabilities,axis=1).reshape((total_img,1))
    probabilities_argmax=np.argmax(probabilities,axis=1).reshape((total_img,1))
    index_sort=np.argsort(probabilities_max,axis=0).reshape((total_img,1))[::-1,:][:num_img:].reshape((num_img)).tolist()
    probabilities_max_sort=probabilities_max[index_sort,:]
    #accuracy = np.average(np.equal(probabilities_argmax[index_sort,:], np.argmax(labels[index_sort,:], 1)).astype(np.float32))
    accuracy = np.average(np.equal(np.argmax(probabilities[index_sort,:], 1), np.argmax(labels[index_sort,:], 1)).astype(np.float32))
    print('top '+str(fraction)+' of images include '+str(num_img)+' images')
    print('the min probablity is: '+str(probabilities_max_sort[-1][0]))
    print('accuracy is: '+str(accuracy))


def evaluate():
    sess=tf.InteractiveSession()
    saver = tf.train.import_meta_graph('save/max/model.ckpt.meta')
    saver.restore(sess, "save/max/model.ckpt")
    x = tf.get_collection('x')[0]
    keep_prob_fc1 = tf.get_collection('keep_prob_fc1')[0]
    keep_prob_fc2 = tf.get_collection('keep_prob_fc2')[0]
    y_conv = tf.get_collection('y_conv')[0]
    is_training=tf.get_collection('is_training')[0]
    current_epoch = tf.get_collection('current_epoch')[0].eval()
    print('epoch when save : '+str(current_epoch))

    #public_test_images = np.load('file npy/test/images.npy')
    #public_test_labels = np.load('file npy/test/labels.npy')
    #print('test :')
    #rs1=compute_accuracy_in_test(public_test_images,public_test_labels,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training)
    print('valid :')
    validation_images = np.load('file npy/valid/images.npy')
    validation_labels = np.load('file npy/valid/labels.npy')
    #compute_accuracy_in_test(validation_images, validation_labels,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training)
    compute_accuracy_in_test_top(validation_images, validation_labels,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,0.8)


if __name__ == "__main__":
    evaluate()