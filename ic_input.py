import numpy as np
import os
import random
import math
from skimage.transform import rotate
from skimage.transform import resize
from skimage import img_as_float



def center_crop(x_batch,crop_size):
    width = np.shape(x_batch)[1]
    x = (width - crop_size) // 2
    y = (width - crop_size) // 2
    return [x_batch[:,x:x + crop_size, y:y + crop_size,:]]


def ten_crop(x_batch,crop_size):
    width = np.shape(x_batch)[1]
    x = (width - crop_size) // 2
    y = (width - crop_size) // 2
    batch_center = x_batch[:, x:x + crop_size, y:y + crop_size,:]
    batch_topleft = x_batch[:, :crop_size:, :crop_size:,:]
    batch_topright = x_batch[:, width-crop_size:width:, :crop_size:,:]
    batch_downright = x_batch[:, width-crop_size:width:, width-crop_size:width:,:]
    batch_downleft = x_batch[:, :crop_size:, width-crop_size:width:,:]

    #batch_next1 = x_batch[:, :42:, y:y + 42]
    #batch_next2 = x_batch[:, x:x + 42, 6:48:]
    #batch_next3 = x_batch[:, 6:48:, y:y + 42]
    #batch_next4 = x_batch[:, x:x + 42, :42:]

    return batch_center, batch_topleft, batch_topright, batch_downright, batch_downleft, \
           batch_center[:, :, ::-1,:], batch_topleft[:, :, ::-1,:], batch_topright[:, :, ::-1,:], batch_downright[:, :, ::-1,:], \
           batch_downleft[:, :, ::-1,:]

def eighteen_crop(x_batch,crop_size):
    width=np.shape(x_batch)[1]
    x = (width - crop_size) // 2
    y = (width - crop_size) // 2
    batch_center = x_batch[:, x:x + crop_size: , y:y + crop_size:,:]
    batch_topleft = x_batch[:, :crop_size:, :crop_size:,:]
    batch_topright = x_batch[:, width-crop_size:width:, :crop_size:,:]
    batch_downright = x_batch[:, width-crop_size:width:, width-crop_size:width:,:]
    batch_downleft = x_batch[:, :crop_size:, width-crop_size:width:,:]

    batch_next1 = x_batch[:, :crop_size:, y:y + crop_size,:]
    batch_next2 = x_batch[:, x:x + crop_size, width-crop_size:width:,:]
    batch_next3 = x_batch[:, width-crop_size:width:, y:y + crop_size,:]
    batch_next4 = x_batch[:, x:x + crop_size, :crop_size:,:]

    return batch_center, batch_topleft, batch_topright, batch_downright, batch_downleft, \
    batch_center[:, :, ::-1,:], batch_topleft[:, :, ::-1,:], batch_topright[:, :, ::-1,:], batch_downright[:, :, ::-1,:], \
    batch_downleft[:, :, ::-1,:], \
    batch_next1, batch_next2, batch_next3, batch_next4, \
    batch_next1[:, :, ::-1,:], batch_next2[:, :, ::-1,:], batch_next3[:, :, ::-1,:], batch_next4[:, :, ::-1,:]

def more_crop(x_batch,crop_size):
    list_crop=[]
    for batch in resize_bacth(x_batch, 110, False):
        batches = eighteen_crop(batch,crop_size)
        list_crop.extend(batches)
    for batch in resize_bacth(x_batch, 120, False):
        batches = eighteen_crop(batch,crop_size)
        list_crop.extend(batches)
    for batch in resize_bacth(x_batch, 130, False):
        batches = eighteen_crop(batch,crop_size)
        list_crop.extend(batches)
    return list_crop
'''
def augmentation_task(img,pca): #img have 3 dimension heigh,width,chanel
    random_mirror = random.randint(1, 2)
    angle = random.randint(-45, 45)
    center = (random.randint(57, 62), random.randint(57, 62))
    width = random.randint(110, 130)
    size = (width, width)
    crop_size=96
    # random color jittering
    #pertub=get_pertub(pca)
    #img=img+pertub
    # mirror
    if random_mirror == 1:
        img = img[:, ::-1,:]
    # rotate
    img = img_as_float(rotate(img, angle, center=center))
    # resize
    img = img_as_float(resize(img, size))
    # crop image to size (42,42)
    x = random.randint(0, width - crop_size)
    y = random.randint(0, width - crop_size)
    img = img[x:x + crop_size, y:y + crop_size,:]
    return img
'''

def augmentation_task(img,pca): #img have 3 dimension heigh,width,chanel
    type_augment=random.randint(1, 3)
    width = 120
    if type_augment==1:# mirror
        random_mirror = random.randint(1, 2)
        if random_mirror == 1:
            img = img[:, ::-1,:]
    elif type_augment==2: # rotate
        angle = random.randint(-45, 45)
        img = img_as_float(rotate(img, angle))
    elif type_augment==3:# resize
        width = random.randint(110, 130)
        size = (width, width)
        img = img_as_float(resize(img, size))
    # random color jittering
    #pertub=get_pertub(pca)
    #img=img+pertub
    # crop
    crop_size = 96
    x = random.randint(0, width - crop_size)
    y = random.randint(0, width - crop_size)
    img = img[x:x + crop_size, y:y + crop_size,:]
    return img

def augmentation_batch(x_batch,pca):
    num_examples = np.shape(x_batch)[0]
    new_img = []
    for i in range(num_examples):
        img = x_batch[i]
        new_img.append(augmentation_task(img,pca))
    return np.array(new_img)


def pre_processing(x_batch,X_mean,X_std):
    crop_size = 96
    x_batch=x_batch.reshape((-1, crop_size * crop_size * 3))
    x_batch = (x_batch - X_mean) / X_std
    x_batch = x_batch.reshape((-1, crop_size, crop_size, 3))
    return x_batch

def resize_bacth(x_batch, width, is_dense):
    size = (width, width)
    origin_width=np.shape(x_batch)[1]
    # centers=[(23,23),(23,21),(21,23),(23,26),(26,23)]
    num_examples = np.shape(x_batch)[0]
    list_new_img = []
    for j in range(9):
        new_img = []
        for i in range(num_examples):
            img = x_batch[i]
            img = img_as_float(rotate(img, -45 + j * 11.25, center=(int((origin_width-1)/2),int((origin_width-1)/2))))
            img = img_as_float(resize(img, size))
            new_img.append(img)
        new_img = np.array(new_img)
        list_new_img.append(new_img)
        if is_dense == True:
            list_new_img.append(new_img[:, :, ::-1])
    return list_new_img


def shuffle_data(train_images,train_labels):
    arr = list(range(len(train_labels)))
    random.shuffle(arr)
    train_images = train_images[arr]
    train_labels = train_labels[arr]
    return train_images,train_labels

def get_sets():
    train_images = np.load('file npy/train/images.npy')
    train_labels = np.load('file npy/train/labels.npy')
    valid_images = np.load('file npy/valid/images.npy')
    valid_labels = np.load('file npy/valid/labels.npy')
    return train_images,train_labels,valid_images,valid_labels

def pca(X):
    "X have shape (-1,100,100,3) and have been mean subtracted "
    shape=np.shape(X)
    X_reshape=np.reshape(np.copy(X),(shape[0]*shape[1]*shape[2],shape[3]))
    X_reshape-=np.mean(X_reshape,0)
    cov = np.dot(X_reshape.T, X_reshape) / X_reshape.shape[0]
    U, s, V = np.linalg.svd(cov)
    s=np.reshape(s,(1,shape[3]))
    s=np.sqrt(s)
    pca = np.sqrt(s) * U
    np.save('file npy/pca.npy',pca)
    return pca

def get_pertub(pca):
    "return pertub have size (3,)"
    mean=0.
    std=0.1
    shape=np.shape(pca)
    pertub= np.sum(pca * np.random.rand(1,shape[1]) * std,axis=1)
    return pertub
