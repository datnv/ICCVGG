import numpy as np
import math
import os
import cv2
import random
import ic_input


NG = 'data/NG'#
OK = 'data/OK'

img_size=120
crop_size=96

train_fraction=0.75
valid_fraction=0.25
test_fraction=0.

global X_mean,X_std

def statistic(Y,type):
    print(type)
    print('num images : '+str(np.shape(Y)[0]))
    ng=0
    ok=0
    for i in range(np.shape(Y)[0]):
        if Y[i][0]==1.0:
            ng+=1
        else:
            ok+=1
    print('num NG images : '+str(ng))
    print('num OK images : ' + str(ok))
'''
def pre_processing(X,type):
    """X have size (-1,100*100*3)"""
    global X_mean
    global X_std
    if type == "train":
        X_mean=np.mean(X, axis=0).reshape((1, img_size*img_size*3))
        X_std=np.std(X, axis=0).reshape((1, img_size*img_size*3))
    X=(X-X_mean)/X_std
    X = X.reshape((-1, img_size, img_size,3))
    return X
'''

def compute_mean(X,type):
    """X have size (-1,120*120*3)"""
    global X_mean
    global X_std
    X = X.reshape((-1, img_size, img_size, 3))
    X_crop=ic_input.center_crop(X,crop_size)[0]
    X_crop = X_crop.reshape((-1, crop_size* crop_size* 3))
    if type == "train":
        X_mean = np.mean(X_crop, axis=0).reshape((1, crop_size * crop_size * 3))
        X_std = np.std(X_crop, axis=0).reshape((1, crop_size * crop_size * 3))
    #X_mean = X_mean.reshape((-1, crop_size, crop_size, 3))
    #X_std = X_std.reshape((-1, crop_size, crop_size, 3))
    #X = X.reshape((-1, img_size, img_size,3))
    return X


def read_and_split():
    X=[]
    Y=[]
    nb = 0
    # read NG images
    for subfolder in os.listdir(NG):
        for image in os.listdir(NG + '/' + subfolder):
            img = cv2.imread(NG + '/' + subfolder + '/' + image) #read bgr image
            if len(np.shape(img)) == 3:
                img = cv2.resize(img, (img_size, img_size), interpolation=cv2.INTER_CUBIC)
                img=img[:,:,::-1] #convert to RGB
                img = np.reshape(img,(img_size*img_size*3))
                X.append(img)
                Y.append([1.0,0.0])
                nb += 1
    X=np.array(X)
    Y=np.array(Y)
    index_list = list(np.random.permutation(len(X)))
    for i in range(5):
        index_list = np.random.permutation(index_list)
    X=X[index_list]
    Y=Y[index_list]
    train_images=  X[:int(math.ceil((train_fraction)*len(X)))]
    train_labels=Y[:int(math.ceil((train_fraction)*len(Y)))]
    valid_images=X[int(math.ceil((train_fraction)*len(X))) : int(math.ceil((train_fraction+valid_fraction)*len(X)))]
    valid_labels = Y[int(math.ceil((train_fraction) * len(Y))): int(math.ceil((train_fraction+valid_fraction) * len(Y)))]
    test_images=X[int(math.ceil((train_fraction+valid_fraction)*len(X))) : ]
    test_labels = Y[int(math.ceil((train_fraction+valid_fraction) * len(Y))):]

    # read OK images
    X=[]
    Y=[]
    for subfolder in os.listdir(OK):
        for image in os.listdir(OK + '/' + subfolder):
            img = cv2.imread(OK + '/' + subfolder + '/' + image)
            if len(np.shape(img)) == 3:
                img = cv2.resize(img, (img_size, img_size), interpolation=cv2.INTER_CUBIC)
                img = img[:, :, ::-1]  # convert to RGB
                img = np.reshape(img, (img_size * img_size * 3))
                X.append(img)
                Y.append([0.0, 1.0])
                nb += 1
    X=np.array(X)
    Y=np.array(Y)
    index_list = list(np.random.permutation(len(X)))
    for i in range(5):
        index_list = np.random.permutation(index_list)
    X=X[index_list]
    Y=Y[index_list]
    train_images=np.append(train_images, X[:int(math.ceil((train_fraction)*len(X)))] , axis=0 ).astype(np.float64)
    train_labels=np.append(train_labels ,Y[:int(math.ceil((train_fraction)*len(Y)))] , axis=0)
    valid_images=np.append(valid_images ,X[int(math.ceil((train_fraction)*len(X))) : int(math.ceil((train_fraction+valid_fraction)*len(X)))],axis=0).astype(np.float64)
    valid_labels=np.append(valid_labels,Y[int(math.ceil((train_fraction) * len(Y))): int(math.ceil((train_fraction+valid_fraction) * len(Y)))],axis=0)
    test_images=np.append(test_images,X[int(math.ceil((train_fraction+valid_fraction)*len(X))) : ],axis=0).astype(np.float64)
    test_labels=np.append(test_labels,Y[int(math.ceil((train_fraction+valid_fraction) * len(Y))):],axis=0)
    print('nb :'+str(nb))
    return train_images,train_labels,valid_images,valid_labels,test_images,test_labels


if __name__ == "__main__":
    train_images, train_labels, valid_images, valid_labels, test_images, test_labels=read_and_split()

    train_images = compute_mean(train_images,'train')
    valid_images = compute_mean(valid_images,'valid')
    test_images = compute_mean(test_images,'test')

    statistic(train_labels,'train')
    statistic(valid_labels,'valid')
    statistic(test_labels,'test')

    np.save('file npy/X_mean.npy',X_mean)
    np.save('file npy/X_std.npy',X_std)
    np.save('file npy/train/images.npy', train_images)
    np.save('file npy/train/labels.npy',train_labels)
    np.save('file npy/valid/images.npy',valid_images)
    np.save('file npy/valid/labels.npy',valid_labels)
    np.save('file npy/test/images.npy',test_images)
    np.save('file npy/test/labels.npy',test_labels)
