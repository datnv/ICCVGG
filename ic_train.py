import ic
import ic_input
import ic_eval
import numpy as np
import os
import tensorflow as tf
import time


nb_max_epochs = 1000#
batch_size = 128
drop_out_prob = 0.5

def compute_accuracy_in_valid(epoch, step,images,labels,y_conv,x,keep_prob_fc1,keep_prob_fc2,validation_acc,is_training,X_mean,X_std):
    sum = 0
    num = 0
    result=[]
    for i in range(11):
        x_batch = images[i * (len(images) // 10):(i + 1) * (len(images) // 10)]
        y_batch = labels[i * (len(images) // 10):(i + 1) * (len(images) // 10)]
        y_predict=ic_eval.get_emotion_ten_crops(x_batch,y_conv,x,keep_prob_fc1,keep_prob_fc2,is_training,X_mean,X_std)
        correct_prediction = np.equal(np.argmax(y_predict, 1), np.argmax(y_batch, 1)).astype(np.float32)
        sum += np.sum(correct_prediction)
        num += np.size(correct_prediction)
    acc = sum / num
    validation_acc.append([epoch, acc])
    np.save('save/current/validation_acc.npy', validation_acc)
    print('epoch ' + str(epoch + 1) + ' : accuracy in validation = ' +
          str(acc))
    return acc

def train():
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.75)
    sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))
    X_mean=np.load('file npy/X_mean.npy')
    X_std = np.load('file npy/X_std.npy')

    if not os.path.isfile('save/current/model.ckpt.index'):  # not load
        print('not load parameters')
        x, y_ = ic.input()
        y_conv = ic.inference(x)
        ic.define_additional_variables()
        #loss = ic.svm_loss(y_conv,y_)
        loss = ic.softmax_loss(y_conv,y_)
        train_step = ic.train(loss)
        validation_acc = []
        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()
    else:
        print('load parameters')
        saver = tf.train.import_meta_graph('save/current/model.ckpt.meta')
        saver.restore(sess, "save/current/model.ckpt")
        validation_acc = np.load('save/current/validation_acc.npy').tolist()

    origin_loss = tf.get_collection('origin_loss')[0]
    regul_loss = tf.get_collection('regul_loss')[0]
    total_loss = tf.get_collection('total_loss')[0]
    accuracy = tf.get_collection('batch_accuracy')[0]
    learning_rate = tf.get_collection('learning_rate')[0]
    train_step = tf.get_collection('train_step')[0]
    x = tf.get_collection('x')[0]
    y_ = tf.get_collection('y_')[0]
    keep_prob_fc1 = tf.get_collection('keep_prob_fc1')[0]
    keep_prob_fc2 = tf.get_collection('keep_prob_fc2')[0]
    y_conv = tf.get_collection('y_conv')[0]
    is_training = tf.get_collection('is_training')[0]
    current_epoch = tf.get_collection('current_epoch')[0]
    current_step = tf.get_collection('current_step')[0]
    max_acc_tensor = tf.get_collection('max_acc_tensor')[0]
    loss_summary_placeholder = tf.get_collection('loss_summary_placeholder')[0]
    acc_train_placeholder = tf.get_collection('acc_train_placeholder')[0]
    summary_op = tf.get_collection('summary_op')[0]
    train_writer = tf.summary.FileWriter('summary/train')


    train_images, train_labels, validation_images, validation_labels = ic_input.get_sets()
    pca=ic_input.pca(train_images)
    train_images, train_labels = ic_input.shuffle_data(train_images, train_labels)

    compute_accuracy_in_valid(current_epoch.eval(), -1,validation_images,validation_labels,\
                              y_conv,x,keep_prob_fc1,keep_prob_fc2,validation_acc,is_training,X_mean,X_std)

    max_acc = max_acc_tensor.eval()
    print('max_acc : ' + str(max_acc))
    step = current_step.eval()
    min_ttl = 100000000.
    repeat = 0  # dem so lan accuracy tren tap valid khong tang
    lnr = learning_rate.eval()
    for epoch in range(current_epoch.eval() + 1, nb_max_epochs):
        start = time.time()
        average_hgl = []
        average_rel = []
        average_ttl = []
        average_ac = []
        for i in range(len(train_images) // batch_size + 1):
            step += 1
            x_batch = train_images[i * batch_size:(i + 1) * batch_size]
            y_batch = train_labels[i * batch_size:(i + 1) * batch_size]
            x_batch = ic_input.augmentation_batch(x_batch,pca)
            x_batch = ic_input.pre_processing(x_batch,X_mean,X_std)

            hgl, rel, ttl, ac, _ = sess.run([origin_loss, regul_loss, total_loss, accuracy, train_step], \
                                            feed_dict={x: x_batch, y_: y_batch, keep_prob_fc1: 1 - drop_out_prob,
                                                       keep_prob_fc2: 1 - drop_out_prob, is_training:True})
            average_hgl.append(hgl)
            average_rel.append(rel)
            average_ttl.append(ttl)
            average_ac.append(ac)
        average_hgl = np.average(average_hgl)
        average_rel = np.average(average_rel)
        average_ttl = np.average(average_ttl)
        average_ac = np.average(average_ac)
        end = time.time()
        durtime = end - start
        print(
            'average total loss: ' + str(average_ttl) + ' , origin loss: ' + str(average_hgl) + ' , regul loss: ' + str(
                average_rel)+ ' , acc train: '+str(average_ac)
            + ' , lnr: ' + str(lnr)+' , time: '+str(durtime)+'s')
        summary = sess.run(summary_op, feed_dict={loss_summary_placeholder: average_ttl, \
                                                  acc_train_placeholder: average_ac, x: x_batch})
        train_writer.add_summary(summary, global_step=epoch)
        if epoch % 10 == 0:
            acc = compute_accuracy_in_valid(epoch, step,validation_images,validation_labels,\
                              y_conv,x,keep_prob_fc1,keep_prob_fc2,validation_acc,is_training,X_mean,X_std)
            #acc = np.average(np.array(validation_acc[-10::])[:, 1])  # tinh trung binh 10 accuracy gan nhat tren validation
            #print('average acc in validation : ' + str(acc))
            sess.run(tf.assign(current_epoch, epoch))
            sess.run(tf.assign(current_step, step))
            if acc >= max_acc:
                max_acc = acc
                sess.run(tf.assign(max_acc_tensor, max_acc))
                saver.save(sess, "save/max/model.ckpt")
            saver.save(sess, "save/current/model.ckpt")

        # consider change learning rate
        if average_ttl < min_ttl:
            repeat = 0
            min_ttl = average_ttl
        else:
            repeat += 1

        if repeat >= 30:
            repeat = 0
            min_ttl = average_ttl
            lnr /= 10.
            sess.run(tf.assign(learning_rate, lnr))
            print('change learning rate = ' + str(lnr))
        train_images, train_labels = ic_input.shuffle_data(train_images, train_labels)

if __name__ == "__main__":
    train()