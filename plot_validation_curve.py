import tensorflow as tf
import numpy as np

def write_summary(array,summ_op,writer):#
    for i in range(len(array)):
        pair = array[i]
        step = pair[0]
        acc = pair[1]
        print(str(step) + ' : ' + str(acc))
        summary = sess.run(summ_op, feed_dict={accuracy: acc})
        writer.add_summary(summary, global_step=step)

sess=tf.InteractiveSession()

validation_acc=np.load('save/current/validation_acc.npy')
accuracy=tf.placeholder(dtype=tf.float32)
summary_valid_op=tf.summary.scalar('accuracy',accuracy)
validation_writer=tf.summary.FileWriter('summary/validation')

write_summary(validation_acc,summary_valid_op,validation_writer)

print('done')